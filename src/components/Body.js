import React, {Component} from 'react';
import { css } from 'emotion'
import { Button, Form, FormGroup, Label, Input, FormFeedback, Table } from 'reactstrap';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submitData: {
                selectedMeal: 'breakfast',
                totalPeople: 1,
                validTotalPeople: true,
                selectedRestaurant: '',
                countDish: 0,
                dishes: []
            }
        };

        this.updateMeal = this.updateMeal.bind(this);
        this.updateRestaurant = this.updateRestaurant.bind(this);
        this.updateTotalPeople = this.updateTotalPeople.bind(this);
        this.addDish = this.addDish.bind(this);
        this.removeDish = this.removeDish.bind(this);
        this.updateDishId = this.updateDishId.bind(this);
        this.updateDishQuantity = this.updateDishQuantity.bind(this);
        this.onKeyPressTotalPeople = this.onKeyPressTotalPeople.bind(this);
        this.onBlurTotalPeople = this.onBlurTotalPeople.bind(this);
        this._updateSubmitData = this._updateSubmitData.bind(this);

        this.setDefaultDish = this.setDefaultDish.bind(this);
    }

    setDefaultDish(tempSubmitData){
        let dish = this.props.data.fullData.filter(item => item.restaurant === this.state.submitData.selectedRestaurant && item.availableMeals.indexOf(this.state.submitData.selectedMeal) >= 0)[0];
        tempSubmitData.dishes.push({
            id: tempSubmitData.countDish,
            dishId: dish.id,
            name: dish.name,
            quantity: 1
        });
        tempSubmitData.countDish++;
        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    updateMeal(event){
        let tempSubmitData = this.state.submitData;
        tempSubmitData.selectedMeal = event.target.value;
        if(this.props.data.byMeals[tempSubmitData.selectedMeal].length > 0) {
            tempSubmitData.selectedRestaurant = this.props.data.byMeals[tempSubmitData.selectedMeal][0];
            tempSubmitData.countDish = 0;
            tempSubmitData.dishes = [];
            this.setDefaultDish(tempSubmitData);
        }

        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    updateRestaurant(event){
        let tempSubmitData = this.state.submitData;
        tempSubmitData.selectedRestaurant = event.target.value;
        //reset dishes if update restaurant
        tempSubmitData.countDish = 0;
        tempSubmitData.dishes = [];
        this.setDefaultDish(tempSubmitData);
    }

    updateTotalPeople(event){
        let tempSubmitData = this.state.submitData;
        tempSubmitData.totalPeople = parseInt(event.target.value);
        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    onKeyPressTotalPeople(event){
        if(event.key === 'Enter') event.preventDefault();
        if(event.key === 'e') event.preventDefault();
    }

    onBlurTotalPeople(event){
        let tempSubmitData = this.state.submitData;
        if(event.target.value < 1 || event.target.value > 10){
            tempSubmitData.validTotalPeople = false;
        } else {
            tempSubmitData.validTotalPeople = true;
        }
        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    updateDishId(serveId, e){
        let tempSubmitData = this.state.submitData;
        console.log(this.props.data.fullData, e.target.value);
        for(let i = 0 ; i < tempSubmitData.dishes.length; i++){
            if(tempSubmitData.dishes[i].id == serveId){
                tempSubmitData.dishes[i].dishId = e.target.value;
                tempSubmitData.dishes[i].name = this.props.data.fullData.filter(t => t.id === parseInt(e.target.value))[0].name;
                break;
            }
        }

        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    updateDishQuantity(serveId, e){
        let tempSubmitData = this.state.submitData;
        for(let i = 0 ; i < tempSubmitData.dishes.length; i++){
            if(tempSubmitData.dishes[i].id == serveId){
                tempSubmitData.dishes[i].quantity = parseInt(e.target.value);
                break;
            }
        }

        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    addDish(){
        let tempSubmitData = this.state.submitData;
        this.setDefaultDish(tempSubmitData);
    }

    removeDish(serveId){
        let tempSubmitData = this.state.submitData;
        tempSubmitData.dishes = tempSubmitData.dishes.filter(item => item.id != serveId);
        this.setState({submitData: tempSubmitData});
        this._updateSubmitData(this.state.submitData);
    }

    _updateSubmitData(submitData){
        this.props.updateSubmitData(submitData);
    }

    componentDidMount() {
        this._updateSubmitData(this.state.submitData);
    }

    render() {
        const body = css`
          padding-top: 50px;
        `
        const button = css`
          margin-left: 10px;
        `
        return (
            <div className={body}>
                {this.props.step === 1 ?
                    <Form>
                        <FormGroup>
                            <Label>Please select a meal</Label>
                            <Input type="select" name="select" onChange={this.updateMeal} value={this.state.submitData.selectedMeal}>
                                {
                                    this.props.data.meals.map((item) => (
                                        <option key={item}>{item}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>

                        <FormGroup>
                            <Label>Please enter number of people</Label>
                            <Input invalid={!this.state.submitData.validTotalPeople}
                                   type="number"
                                   min={1}
                                   max={10}
                                   pattern="[0-9]*"
                                   onChange={this.updateTotalPeople}
                                   onKeyPress={this.onKeyPressTotalPeople}
                                   onBlur={this.onBlurTotalPeople}
                                   value={this.state.submitData.totalPeople}></Input>
                            <FormFeedback>The number of people is invalid(1 to 10 allowed)</FormFeedback>
                        </FormGroup>
                    </Form> : null
                }

                {this.props.step === 2 ?
                    <Form>
                        <FormGroup>
                            <Label>Please select a restaurant</Label>
                            <Input type="select" name="select" onChange={this.updateRestaurant} value={this.state.submitData.selectedRestaurant}>
                                {
                                    this.props.data.byMeals[this.state.submitData.selectedMeal].map((item) => (
                                        <option key={item} value={item}>{item}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>
                    </Form> : null
                }

                {this.props.step === 3 ?
                    this.state.submitData.dishes.map(item => (
                    <Form key={item.id}>
                        <FormGroup>
                            <Label>Please select a dish</Label>
                            <Input type="select" name="select" onChange={(e) => this.updateDishId(item.id, e)} value={item.dishId}>
                                {
                                    this.props.data.fullData.filter(dishes => dishes.restaurant === this.state.submitData.selectedRestaurant && dishes.availableMeals.indexOf(this.state.submitData.selectedMeal) >= 0).map((dish) => (
                                            <option key={dish.id} value={dish.id}>{dish.name}</option>
                                        ))
                                }
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label>Please enter number of people</Label>
                            <Input type="number" value={item.quantity} min={1} max={10} onChange={(e) => this.updateDishQuantity(item.id, e)}></Input>
                        </FormGroup>
                        <Button className={button} color="success" onClick={this.addDish}>+</Button>
                        <Button className={button} color="success" onClick={(e) => this.removeDish(item.id)}>-</Button>
                    </Form>)) : null
                }

                {this.props.step === 4 ?
                    <Table>
                        <tbody>
                        <tr>
                            <td>Meal</td>
                            <td>{this.state.submitData.selectedMeal}</td>
                        </tr>
                        <tr>
                            <td>No of People</td>
                            <td>{this.state.submitData.totalPeople}</td>
                        </tr>
                        <tr>
                            <td>Restaurant</td>
                            <td>{this.state.submitData.selectedRestaurant}</td>
                        </tr>
                        <tr>
                            <td>Dishes</td>
                            <td>
                            {
                                this.state.submitData.dishes.map((item) =>(
                                    <p key={item.id}>{item.name}: <span>{item.quantity}</span></p>
                                ))
                            }
                            </td>
                        </tr>
                        </tbody>
                    </Table> : null
                }
            </div>
        )
    }
}

export default Body;