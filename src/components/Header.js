import React, {Component} from 'react';
import { css } from 'emotion'
import { Button } from 'reactstrap';

class Header extends Component {
    constructor(props) {
        super(props);

        let steps = [];
        for(let i = this.props.minStep; i <= this.props.maxStep; i++) steps.push(i);

        this.state = {
            steps: steps
        };

        this.toStep = this.toStep.bind(this);
    }

    toStep(step){
        this.props._toStep(step);
    }

    render() {
        const button = css`
          margin-left: 10px;
        `
        return (
            this.state.steps.map(step => (
                <Button color="primary" className={button} key={step} onClick={(e) => {this.toStep(step)}} active={this.props.step === step} disabled={this.props.step < step}>
                    {this.props.maxStep === step ? 'PREVIEW' : 'STEP ' + step}
                </Button>
            ))
        )
    }
}

export default Header;