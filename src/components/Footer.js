import React, {Component} from 'react';
import { css } from 'emotion'
import { Button } from 'reactstrap';

class Footer extends Component {
    constructor(props) {
        super(props);

        this.nextStep = this.nextStep.bind(this);
        this.prevStep = this.prevStep.bind(this);
    }

    nextStep(){
        this.props._nextStep();
    }

    prevStep(){
        this.props._prevStep();
    }

    render() {
        const footer = css`
          padding-top: 50px;
        `
        return (
            <div className={footer}>
                <Button className="float-left" color="warning" onClick={this.prevStep} disabled={this.props.step === this.props.minStep}>PREVIOUS</Button>
                <Button className="float-right" color="warning" onClick={this.nextStep}>{this.props.step === this.props.maxStep ? 'SUBMIT' : 'NEXT'}</Button>
            </div>
        )
    }
}

export default Footer;