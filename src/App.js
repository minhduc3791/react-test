import React, { Component } from 'react';
import { css } from 'emotion'
import './App.css';
import Header from "./components/Header";
import Body from "./components/Body";
import Footer from "./components/Footer";
import { Alert } from 'reactstrap';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            //config
            minStep: 1,
            maxStep: 4,
            //end config
            step: 1,
            data: {
                meals: [],
                byMeals: {},
                fullData: []
            },
            submitData: {},
            noRestaurantAt: '',
            noRestaurant: false,
            noDish: false,
            duplicateDish: false,
            notEnoughDish: false
        };

        this.nextStep = this.nextStep.bind(this);
        this.prevStep = this.prevStep.bind(this);
        this.toStep = this.toStep.bind(this);

        this.updateSubmitData = this.updateSubmitData.bind(this);
        this.preprocessing = this.preprocessing.bind(this);

        this.onDismiss = this.onDismiss.bind(this);
    }

    nextStep(){
        let tempSubmitData = this.state.submitData;
        if(this.state.step === 1){
            if(this.state.submitData.validTotalPeople) {
                if(this.state.data.byMeals[this.state.submitData.selectedMeal].length > 0) {
                    //default to 1st restaurant can serve the meal
                    tempSubmitData.selectedRestaurant = this.state.data.byMeals[tempSubmitData.selectedMeal][0];
                    this.setState({submitData: tempSubmitData});

                    this.setState({step: Math.min(this.state.maxStep, this.state.step + 1)});
                    this.setState({noRestaurant: false});
                } else {
                    this.setState({noRestaurantAt: this.state.submitData.selectedMeal});
                    this.setState({noRestaurant: true});
                }
            }
        } else if(this.state.step === 2){
            this.setState({step: Math.min(this.state.maxStep, this.state.step + 1)});
        } else if(this.state.step === 3){
            let dupl = false;
            this.setState({duplicateDish: false});

            for(let i = 0 ; i < this.state.submitData.dishes.length; i++){
                 if(this.state.submitData.dishes.filter(dish => dish.dishId == this.state.submitData.dishes[i].dishId).length > 1){
                     this.setState({duplicateDish: true});
                     dupl = true;
                     break;
                 }
            }

            if(!dupl) {
                let notEnough = false;
                this.setState({notEnoughDish: false});
                let total = 0;
                for(let i = 0 ; i < this.state.submitData.dishes.length; i++){
                    total += this.state.submitData.dishes[i].quantity;
                }
                if(total < this.state.submitData.totalPeople){
                    notEnough = true;
                    this.setState({notEnoughDish: true});
                } else {
                    this.setState({step: Math.min(this.state.maxStep, this.state.step + 1)});
                }
            }
        } else if(this.state.step === 4){
            console.log(this.state.submitData);
        }
    }

    prevStep(){
        this.setState({step: Math.max(this.state.minStep, this.state.step - 1)});
    }

    toStep(step){
        this.setState({step: step});
    }

    updateSubmitData(submitData){
        this.setState({submitData: submitData});
    }

    onDismiss() {
        this.setState({ noRestaurant: false });
        this.setState({ noDish: false });
        this.setState({ duplicateDish: false });
        this.setState({ notEnoughDish: false });
    }

    preprocessing(data){
        let tempData = {
            meals: ['breakfast', 'lunch', 'dinner'],
            byMeals: {}
        };

        //get all meal
        /*for(let i = 0 ; i < data.length; i++){
            for(let j = 0; j < data[i].availableMeals.length; j++){
                if(tempData.meals.indexOf(data[i].availableMeals[j]) < 0)
                    tempData.meals.push(data[i].availableMeals[j]);
            }
        }*/

        for(let m of tempData.meals){
            tempData.byMeals[m] = [];
        }

        //get all restaurant by meal
        for(let i = 0 ; i < data.length; i++){
            for(let m of tempData.meals){
                //check if this dish is serve at meal m
                if(data[i].availableMeals.indexOf(m) >= 0){
                    //check if restaurant already imported
                    if(tempData.byMeals[m].indexOf(data[i].restaurant) < 0){
                        tempData.byMeals[m].push(data[i].restaurant);
                    }
                }
            }
        }

        tempData.fullData = data;

        let tempSubmitData = this.state.submitData;
        if(tempData.byMeals[tempSubmitData.selectedMeal].length > 0) {
            tempSubmitData.selectedRestaurant = tempData.byMeals[tempSubmitData.selectedMeal][0];
            tempSubmitData.countDish = 1;
            let dish = tempData.fullData.filter(item => item.restaurant === tempSubmitData.selectedRestaurant && item.availableMeals.indexOf(tempSubmitData.selectedMeal) >= 0)[0];
            tempSubmitData.dishes.push({
                id: 0,
                dishId: dish.id,
                name: dish.name,
                quantity: 1
            });
        }

        this.setState({submitData: tempSubmitData});
        this.setState({data: tempData});
    }

    componentDidMount() {
        fetch('http://localhost:3000/data_dishes.json')
            .then(res => res.json())
            .then(data => this.preprocessing(data.dishes))
    }

    render() {
        const container = css`
            padding-left: 100px;
            padding-right: 100px;
        `
        const alert = css`
            height: 60px;
        `
        return (
            <div className="App">
                <div className={alert}>
                    <Alert color="danger" isOpen={this.state.noRestaurant} toggle={this.onDismiss}>
                        Sorry, currently there is no restaurant serve {this.state.noRestaurantAt}, please choose another meal
                    </Alert>
                    <Alert color="danger" isOpen={this.state.noDish} toggle={this.onDismiss}>
                        Sorry, currently there is no dish serve at {this.state.submitData.selectedRestaurant}, please choose another restaurant(or meal)
                    </Alert>
                    <Alert color="danger" isOpen={this.state.duplicateDish} toggle={this.onDismiss}>
                        Sorry, you cannot choose the same dish twice, please check your order
                    </Alert>
                    <Alert color="danger" isOpen={this.state.notEnoughDish} toggle={this.onDismiss}>
                        Sorry, your order is less than total people, did you missing something ?
                    </Alert>
                </div>
                <div className={container}>
                    <Header step={this.state.step} _toStep={this.toStep} maxStep={this.state.maxStep} minStep={this.state.minStep}/>
                    <Body updateSubmitData={this.updateSubmitData} step={this.state.step} data={this.state.data}/>
                    <Footer step={this.state.step} _nextStep={this.nextStep} _prevStep={this.prevStep} maxStep={this.state.maxStep} minStep={this.state.minStep}/>
                </div>
            </div>
        );
    }
}

export default App;
